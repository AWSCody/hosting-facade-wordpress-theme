<?php
/*
 Template Name: Hero Page Template
 */

include dirname(__FILE__) . "/../Core/Display/Display.Class.php";
include dirname(__FILE__) . "/../Core/Filter_Compiler.Class.php";
include dirname(__FILE__) . "/../Include/Common.php";
//include the core functionality for post filtering.
include dirname(__FILE__) . "/../Core/WP-Functions/wp-functions.php";



//Hero Unit Images
$hero_items = array();

$_items = get_post_meta($post -> ID, 'SLIDE', false);


foreach ($_items as $_item) {

	$item = array();
	
	if (strlen($_item) > 0) {
		$_item = explode(";", $_item);
		//echo $_item."<br>\n";
		$item["IMG"] = $_item[0];
		$item["SLIDER_TEXT_H3"] = $_item[1];
		$item["SLIDER_TEXT_H2"] = $_item[2];

		//replace the tags in the item
		$common -> setDataArray($item);
		$item = $common -> compile();

		//add it to the array.
		$hero_items[] = $item;

	}

}

$data["SLIDER_ITEMS"] = $hero_items;


//post preview-image
$data["PREVIEW_IMG"] = "http://placehold.it/350x150";
//$data["PREVIEW_IMG"] = get_post_meta($post -> ID, 'PREVIEW_IMG', true);



//Assign the Page
$data["PAGE"] = "Pages/Hero.html";


$data["PREVIEW_IMG"] = get_post_meta($post -> ID, 'PREVIEW_IMG', true);

$common -> setDataArray($data);
$data = $common -> compile();

Display_Component::renderDisplay(dirname(__FILE__)."/Site", "Site.html", $data);
?>
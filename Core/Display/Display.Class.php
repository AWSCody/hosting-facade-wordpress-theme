<?php
/**
 * Twig Compiler Class
 * Adapted from AWS Framework
 * @author Cody Adkins
 * 6/29/2012
 */

include dirname(__FILE__) . "/Vendor/Twig/Autoloader.php";

Class Display_Component {

	static $twig;

	static $twigTemplates = null;
	//Twig Templates (Array or String) of locations.
	static $exposedFunctions = array();
	//Array of function names, abbreviated and the full name available in the Display.
	/**
	 * Render Display
	 * @param files - The location of the view, or views, <Br>if an array is specified then the view matching the selected_file will be loaded and displayed.
	 * @param selected_file - The selected view file.
	 * @param display_data - Data passed to the template as an associative array.
	 * @param cache - The location of the cache, or null for no cache. If the application is dynamic cache must be set to null.
	 * @param dont_display - If true then dont display the parsed template, just return it as a string value.
	 * @param rules- a list of replace rules to run on the raw html template usefull for updating raw html.<font color="red"> Do not call this every time, use once to make html and then disable the flag!</font>
	 */
	public static function renderDisplay($files, $selected_file, $display_data, $cache = null, $dont_display = false, $rules = NULL) {

		if ($display_data == null) {
			$display_data = array();
		}

		//if the rules are in array format then build the template using the rules.

		if (is_array($rules)) {

			self::updateTemplateURLS($files . "/" . $selected_file, $rules);
		}

		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem($files);
		//$cache = dirname(__FILE__) . "/cache";
		$twig = new Twig_Environment($loader, array('cache' => $cache));
		self::$twig = $twig;
		// hard oveerride of cache.
		//$twig = new Twig_Environment($loader, array('cache' => dirname(__FILE__).'/Cache'));// hard oveerride of cache.

		//Iterate through available functions
		foreach (Display_Component::$exposedFunctions as $fn_name => $fn_context) {
			$twig -> addFunction($fn_name, new Twig_Function_Function($fn_name));
		}

		$template = $twig -> loadTemplate($selected_file);

		if ($dont_display) {
			return $template -> render($display_data);
		} else {
			print($template -> render($display_data));
		}

	}

	public static function exposeFunction($exposed_name, $function_name) {
		self::$exposedFunctions[$exposed_name] = $function_name;

	}

}
?>
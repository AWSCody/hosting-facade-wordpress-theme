<?php

$data["POSTS"] = array();

if (!is_null($data["POST_CATEGORY"])) {
	query_posts("category_name=" . $data["POST_CATEGORY"] . "&order=asc");
	if (have_posts()) :
		while (have_posts()) : the_post();

			global $wp_query;
			$post = $wp_query -> post;
			//read the post meta for the value of FIRST, and push it to the front.
			$post_order = get_post_meta($post -> ID, 'FIRST', true);
			if ($post_order == "true") {
				array_unshift($data["POSTS"],$post);
			}else{
				array_push($data["POSTS"], $post);
			}
			
		endwhile;
	endif;

	//print_r($data["POSTS"]);
}
?>


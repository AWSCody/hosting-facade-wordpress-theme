<?php



Class  Filter_Compiler{

	private $data = array();
	private $filters = array();
	private $filter_opening = "{{";
	private $filter_close = "}}";

	public function setDataArray($arr) {
		$this -> data = $arr;
	}

	public function addData($key, $value) {
		$this -> data[$key] = $value;
	}

	public function addFilter($key, $value) {
		$this -> filters[$key] = $value;
	}

	public function compile() {
		$t_data = array();
		foreach ($this->data as $key => $value) {
			$t_data[$key] = $value;
			foreach ($this->filters as $r_key => $r_value) {
				$t_data[$key] = str_replace($this -> filter_opening . $r_key . $this -> filter_close, $r_value, $t_data[$key]);

			}

		}
		$this -> data = $t_data;
		return $this -> data;
	}

}
?>

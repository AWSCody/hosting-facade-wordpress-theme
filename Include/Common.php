<?php


$common = new  Filter_Compiler();

$data = (!is_array($data) ? array() : $data);

global $wp_query;
$postid = $wp_query -> post -> ID;
$original_post_id = $postid;
$data["SITE_TITLE"] = get_bloginfo("name", $filter);
$data["THEME_URL"] = get_bloginfo("template_url", $filter);
$data["ASSETS_URL"] = get_bloginfo("template_url", $filter)."/Assets";
$data["SITE_URL"] = get_bloginfo("wpurl", $filter);
$data["BASE_URL"] = get_bloginfo("wpurl", $filter);
$data["PAGE_TITLE"] = get_the_title($post -> ID);
$data["PAGE_HEADLINE"] = (is_null($data["PAGE_HEADLINE"])) ? get_post_meta($post -> ID, 'PAGE_HEADLINE', true) : $data["PAGE_HEADLINE"];
$data["CONTENT"] = (is_null($data["CONTENT"])) ? $post -> post_content : $data["CONTENT"];
$data["TIMESTAMP"] = time();

$data["POST_CATEGORY"] = get_post_meta($post -> ID, 'POST_CATEGORY', true);

$common -> addFilter("SITE_URL", $data["SITE_URL"]);
$common -> addFilter("THEME_URL", $data["THEME_URL"]);

$data['NAVIGATION'] = "Common/Navigation.html";
//$data['login'] = "Common/Login.html";

$data['FOOTER'] = "Common/Footer.html";

Display_Component::exposeFunction("dynamic_sidebar", "dynamic_sidebar");
Display_Component::exposeFunction("is_active_sidebar", "is_active_sidebar");
Display_Component::exposeFunction("get_post_meta", "get_post_meta");


?>
<?php


add_theme_support( 'menus' );
add_filter('widget_text', 'do_shortcode');
if ( function_exists('register_sidebar') ) {
register_sidebar(array(
'name' => 'Right Sidebar 1',
'id' => 'right-sidebar-1',
'description' => 'Appears as the sidebar on the right top.',
'before_widget'=> '<div id="%1$s" class="entry sidebar-widget widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3 class="title">',
'after_title' => '</h3>',
));

register_sidebar(array(
'name' => 'Right Sidebar 2',
'id' => 'right-sidebar-2',
'description' => 'Appears as the sidebar on the right middle.',
'before_widget'=> '<div id="%1$s" class="entry sidebar-widget widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h2 class="title">',
'after_title' => '</h2>',
));

register_sidebar(array(
'name' => 'Right Sidebar 3',
'id' => 'right-sidebar-3',
'description' => 'Appears as the sidebar on the right bottom',
'before_widget'=> '<div id="%1$s" class="entry sidebar-widget widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h2 class="title">',
'after_title' => '</h2>' ,
	));
	
	
	register_sidebar(array(
'name' => 'Footer Colum 1',
'id' => 'footer-content-1',
'description' => 'Appears as the sidebar on the bottom left.',
'before_widget'=> '',
'after_widget' => '',
'before_title' => '<h3>',
'after_title' => '</h3>' ,
	));
	
		register_sidebar(array(
'name' => 'Footer Colum 2',
'id' => 'footer-content-2',
'description' => 'Appears as the sidebar on the bottom center.',
'before_widget'=> '',
'after_widget' => '',
'before_title' => '<h3>',
'after_title' => '</h3>' ,
	));
	
		register_sidebar(array(
'name' => 'Footer Colum 3',
'id' => 'footer-content-3',
'description' => 'Appears as the sidebar on the bottom right.',
'before_widget'=> '',
'after_widget' => '',
'before_title' => '<h3>',
'after_title' => '</h3>' ,
	));
	
	}

add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_options_page('My Plugin Options', 'My Plugin', 'manage_options', 'my-unique-identifier', 'my_plugin_options');
}

function my_plugin_options() {
	if (!current_user_can('manage_options')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}
	echo '
	<div class="wrap">
	';
	echo '
	<p>
	Here is where the form would go if I actually had options.
	</p>
	';
	echo '
	</div>';
}
?>